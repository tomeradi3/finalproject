from base_honeypot import HoneyPot


class UserHoneypot(HoneyPot):
    """
    Creates a fake user in the system (could be administrator) and monitors
    all login to that user.
    """