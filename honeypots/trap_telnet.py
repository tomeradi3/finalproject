import socket
import sys
import syslog
import time
import traceback
from threading import Thread


def main():
    start_server()


def start_server():
    host = "127.0.0.1"
    port = 23

    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)   

    try:
        soc.bind((host, port))
    except:
        print("Bind failed. Error : " + str(sys.exc_info()))
        sys.exit()

    soc.listen(5)       
    while True:
        connection, address = soc.accept()
        ip, port = str(address[0]), str(address[1])
        print(ip + ":" + port)

        try:
            Thread(target=client_thread, args=(connection, ip, port)).start()
        except:
            print("Thread did not start.")
            traceback.print_exc()

    soc.close()


def client_thread(connection, ip, port, max_buffer_size = 1024):
    is_active = True
    welcome = b"Welcome to Noisec Honeypot Banner!\n"


    while is_active:

        connection.sendall(welcome)
        time.sleep(1)
        connection.send(b"password:")
        all =""
        who =  ip + ":" + port
        while True:
            data=connection.recv(1024).decode('utf-8')
            all += str(data)
            #print(str(data))
            if data == '\r\n':
                connection.close()
                is_active = False
                print('Received', all)
                
                log = syslog.Syslog("127.0.0.1")
                log.send("Alert on station from: "+str(who)+" msg: "+str(all), syslog.Honeypot.TELNET )
                break;


if __name__ == "__main__":
    main()




            



