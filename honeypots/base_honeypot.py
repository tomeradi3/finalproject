from abc import ABCMeta, abstractmethod


class HoneyPot(metaclass=ABCMeta):
    def __init__(self):
        pass

    @abstractmethod
    def install(self):
        """Installs the honeypot on the system"""
        pass

    @abstractmethod
    def uninstall(self):
        """Uninstalls the honeypot from the system"""
        pass
