import os
import time
import re
import logging
import threading
import syslog
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer

def main():
    def send_syslog():
        old_length = 0
        while True:
            f_handler = open('/home/investigator/Scrivania/pyftpd.log','r')
            time.sleep(3)
            ips = re.findall(r'INFO:pyftpdlib:(Syslog:.*failed login.)',f_handler.read())
            ips = list(set(ips))
            log = syslog.Syslog("127.0.0.1")
            for ip in ips:
                log.send(ip, syslog.Honeypot.FTP )
                #print ip
            f_handler.close()
            f_handler = open('/home/investigator/Scrivania/pyftpd.log','w')
            f_handler.close()
            time.sleep(2)
            
    logging.basicConfig(filename='/home/investigator/Scrivania/pyftpd.log', level=logging.INFO)
    t = threading.Thread(target=send_syslog)
    t.start()
    # Instantiate a dummy authorizer for managing 'virtual' users
    authorizer = DummyAuthorizer()

    # Define a new user having full r/w permissions and a read-only
    # anonymous user
    authorizer.add_user('user', '12345', '.', perm='elradfmwMT')
    #authorizer.add_anonymous(os.getcwd())

    # Instantiate FTP handler class
    handler = FTPHandler
    handler.log_prefix = 'Noisec:[%(username)s]:@%(remote_ip)s#'
    handler.authorizer = authorizer

    # Define a customized banner (string returned when client connects)
    handler.banner = "pyftpdlib based ftpd ready."

    # Specify a masquerade address and the range of ports to use for
    # passive connections.  Decomment in case you're behind a NAT.
    #handler.masquerade_address = '151.25.42.11'
    #handler.passive_ports = range(60000, 65535)

    # Instantiate FTP server class and listen on 0.0.0.0:2121
    address = ('', 21)
    server = FTPServer(address, handler)

    # set a limit for connections
    server.max_cons = 256
    server.max_cons_per_ip = 5

    # start ftp server
    server.serve_forever()

if __name__ == '__main__':
    main()
