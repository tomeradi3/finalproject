"""
Remote syslog client.

Works by sending UDP messages to a remote splunk server. The remote server
must be configured to accept logs from the network.

Author: Harel Elkayam 
"""

import socket
from time import gmtime, strftime
from datetime import datetime
global cur_time
cur_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
class Honeypot:
  "Syslog Honeypot Types"
  USB, USERS, MAIL , TELNET , CAM , FTP = ("HONEYPOT_USB", "HONEYPOT_USERS", "HONEYPOT_MAIL" , "HONEYPOT_TELNET","HONEYPOT_CAMERA","HONEYPOT_FTP")

class Noise:
  "Syslog Noise Types"
  Processes_AV, Processes_V \
  ,SERVICES_AV, SERVICES_VM = ("Processes_AV", "Processes_VM" \
   ,"SERVICES_AV", "SERVICES_VM"     )

class Syslog:
  """A syslog client that logs to a splunk server.

  Example:
  >>> log = syslog.Syslog(host="127.0.0.1")
  >>> log.send("trigger honeypot on station", syslog.Honeypot.USERS )
  """
  def __init__(self,
               host="localhost",
               port=514):
    self.host = host
    self.port = port
    self.Honeypot = Honeypot
    self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

  def send(self, message, name):
    "Send a syslog message to remote host using UDP."
    data = str(cur_time)+" %s:%s" % (name, message)
    print data
    self.socket.sendto(data, (self.host, self.port))






